public class MobilePhone
{
    private String brand;
    private int basePrice;
    private int stockLevel;
    
    public MobilePhone(){
        brand = "unknown";
        basePrice = 100;
        stockLevel = 5;
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getBasePrice(){
        return basePrice;
    }
    
    public void setBasePrice(int newBasePrice){
        if (newBasePrice >= 0)
            basePrice = newBasePrice;        
    }
    
    public int getStockLevel(){
        return stockLevel;
    }
    
    public void setStockLevel(int newStockLevel){
        if (newStockLevel >= 0)
            stockLevel = newStockLevel;
    }
    
    public double discount(double discountRate){
        double discountPrice = basePrice;
        if (discountRate > 0 && discountRate <= 1)
            discountPrice = basePrice - basePrice * discountRate;
        return discountPrice;
    }
    
    // TODO: create a method to restock mobile phones for the shop
    //       the method take the number of phones to be re-stocked as parameter
    //       if the number of phones to be added is positive, then
    //          add one phone to the stock at a time
    //          the method prints the latest stock level after adding each mobile phone
    //       if the number of phones to be added is not positive, then
    //          print an error message
    //       the method prints a message to tell if stock level is sufficient or low
    //          sufficient if there are at least 5 mobile phones in stock
    //          low if there are less than 2 mobile phones in stock

    
    // TODO: (optional) try to write the restock method with a different loop
    
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("basePrice: " + basePrice);
        System.out.println("stockLevel: " + stockLevel);
        if (stockLevel < 2)
            System.out.println("WARNING: stockLevel is low!");
        System.out.println("================================");
    }
}

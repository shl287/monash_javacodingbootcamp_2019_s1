// TODO: import ArrayList class here so you can use ArrayList in you code

public class ShopManager
{
    // TODO: declare an ArrayList of phones representing all phone brands in the shop
    
    
    public ShopManager(){
        // TODO: initialize the ArrayList of phones here
        
    }
    
    // NOTE: ignore non-default constructor, accessor, and mutator for now
    
    public void addNewPhoneBrandToShelf(String initialBrandName, int initialBasePrice, int initialStockLevel){
        //TODO: create a new MobilePhone object
        //      add the newly created MobilePhone object to the ArrayList
        
    }
    
    public void restockExistingPhoneBrand(String brandName, int restockQuantity){
        // TODO: loop through the ArrayList of phone brands (preferably with while loop)
        //          for each phone brand, check if the brand name match the one to restock
        //          if the name match, call the restock method and pass restockQuantity as parameter
        
    }
    
    public void removePhoneBrandFromShelf(String brandToRemove){
        // TODO: loop through the ArrayList of phone brands (preferably with for loop)
        //          for each phone brand, check if the brand name match the one to restock
        //          if the name match, remove the phone brand from the ArrayList
        
    }
    
    public void printAllPhoneBrandDetail(){
        // TODO: loop through the ArrayList of phone brands (preferably with for loop)
        //          for each phone brand, call its printMobileDetail method

    }
    
}

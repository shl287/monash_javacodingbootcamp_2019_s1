public class MobilePhone
{
    private String brand;
    private int basePrice;
    private int stockLevel;
    
    public MobilePhone(){
        brand = "unknown";
        basePrice = 100;
        stockLevel = 5;
    }

    public MobilePhone(String newBrand, int newBasePrice, int newStockLevel){
        brand = newBrand;
        basePrice = newBasePrice;
        stockLevel = newStockLevel;
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getBasePrice(){
        return basePrice;
    }
    
    public void setBasePrice(int newBasePrice){
        if (newBasePrice >= 0)
            basePrice = newBasePrice;        
    }
    
    public int getStockLevel(){
        return stockLevel;
    }
    
    public void setStockLevel(int newStockLevel){
        if (newStockLevel >= 0)
            stockLevel = newStockLevel;
    }
    
    public double discount(double discountRate){
        double discountPrice = basePrice;
        if (discountRate > 0 && discountRate <= 1)
            discountPrice = basePrice - basePrice * discountRate;
        return discountPrice;
    }
    
    public void restock(int newSupply){
        if (newSupply > 0){
            System.out.println("restocking " + brand + "...");
            for (int i = 0; i < newSupply; i++){
                stockLevel++;
                System.out.println("stockLevel increased by 1, current stockLevel: " + stockLevel);
            }
        } else {
            System.out.println("the number of phones to be added should be a positive number!");
        }
        
        String stockLevelLabel = "normal";
        if (stockLevel >= 5)
            stockLevelLabel = "sufficient";
        else if (stockLevel < 2)
            stockLevelLabel = "low";
        
        System.out.println("restock finished, current stock level of " + brand + "is " + stockLevelLabel);
    }
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("basePrice: " + basePrice);
        System.out.println("stockLevel: " + stockLevel);
        if (stockLevel < 2)
            System.out.println("WARNING: stockLevel is low!");
        System.out.println("================================");
    }
}

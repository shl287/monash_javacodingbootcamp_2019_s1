import java.util.ArrayList;

public class ShopManager
{
    private ArrayList<MobilePhone> phoneBrandList;
    
    public ShopManager(){
        phoneBrandList = new ArrayList<MobilePhone>();
    }
    
    public void addNewPhoneBrandToShelf(String brand, int basePrice, int stockLevel){
        phoneBrandList.add(new MobilePhone(brand, basePrice, stockLevel));
    }
    
    public void restockExistingPhoneBrand(String brandToRestock, int restockQuantity){
        for (int i = 0; i < phoneBrandList.size(); i++)
            if (phoneBrandList.get(i).getBrand().equals(brandToRestock))
                phoneBrandList.get(i).restock(restockQuantity);        
    }
    
    public void removePhoneBrandFromShelf(String brandToRemove){
        for (int i = 0; i < phoneBrandList.size(); i++)
            if (phoneBrandList.get(i).getBrand().equals(brandToRemove))
                phoneBrandList.remove(i);
    }
    
    public void printAllPhoneBrandDetail(){
        for (int i = 0; i < phoneBrandList.size(); i++)
            phoneBrandList.get(i).printMobileDetail();
    }
}

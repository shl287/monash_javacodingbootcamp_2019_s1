public class MobilePhone
{
    private String brand;
    // TODO: declare the base price as a new field below
    
    
    public MobilePhone(){
        brand = "unknown";
        // TODO: initialize the base price below
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    // TODO: create accessor method for the base price below
    
    
    // TODO: create mutator method for the base price below
    
    
    // TODO: create disCount methods below, note:
    //       the method should take in the percentage of discount as parameter
    //       the method should return the correct price after considering discount
    
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        // TODO: print the base price on the console below
        
        System.out.println("================================");
    }
}

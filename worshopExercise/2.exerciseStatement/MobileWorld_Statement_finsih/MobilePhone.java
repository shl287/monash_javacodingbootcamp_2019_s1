public class MobilePhone
{
    private String brand;
    private int basePrice;
    
    public MobilePhone(){
        brand = "unknown";
        basePrice = 100;
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getBasePrice(){
        return basePrice;
    }
    
    public void setBasePrice(int newBasePrice){
        basePrice = newBasePrice;
    }
    
    public double discount(double discountRate){
        double discountPrice = 0;
        discountPrice = basePrice - basePrice * discountRate;
        return discountPrice;
    }
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("basePrice: " + basePrice);
        System.out.println("================================");
    }
}

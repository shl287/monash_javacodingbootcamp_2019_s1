public class MobilePhone
{
    private String brand;
    private int basePrice;
    private int stockLevel;
    
    public MobilePhone(){
        brand = "unknown";
        basePrice = 100;
        stockLevel = 5;
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getBasePrice(){
        return basePrice;
    }
    
    public void setBasePrice(int newBasePrice){
        if (newBasePrice >= 0)
            basePrice = newBasePrice;        
    }
    
    public int getStockLevel(){
        return stockLevel;
    }
    
    public void setStockLevel(int newStockLevel){
        if (newStockLevel >= 0)
            stockLevel = newStockLevel;
    }
    
    public double discount(double discountRate){
        double discountPrice = basePrice;
        if (discountRate > 0 && discountRate <= 1)
            discountPrice = basePrice - basePrice * discountRate;
        return discountPrice;
    }
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("basePrice: " + basePrice);
        System.out.println("stockLevel: " + stockLevel);
        if (stockLevel < 2)
            System.out.println("WARNING: stockLevel is low!");
        System.out.println("================================");
    }
}

public class MobilePhone
{
    private String brand;
    private int basePrice;
    // TODO: declare the stock level as a new field below
    
    
    public MobilePhone(){
        brand = "unknown";
        basePrice = 100;
        // TODO: initialize the stock level;
        
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getBasePrice(){
        return basePrice;
    }
    
    public void setBasePrice(int newBasePrice){
        // TODO: check if the new base price is a non-negative number
        //       if yes, change the base price to the new one
        
    }
    
    // TODO: create the accessor method for the stock level field below
    
    
    // TODO: create the mutator method for the stock level field below
    //       make sure that the stock level cannot be set to a negative number
    
    
    public double discount(double discountRate){
        // TODO: check if discountRate is between 0(exclusive) to 1(inclusive)
        //       if yes, then apply the discountRate, and calculate the new price
        //       otherwise, return the base price

    }
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("basePrice: " + basePrice);
        // TODO: print out the stock level
        // TODO: check if the stock level is below 2
        //       if yes, then also print a message that says stock level is low
        
        System.out.println("================================");
    }
}

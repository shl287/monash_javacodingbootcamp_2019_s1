public class MobilePhone
{
    private String brand;
    
    public MobilePhone(){
        brand = "unknown";
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public void printMobileDetail(){
        System.out.println("==== printing Mobile Detail ====");
        System.out.println("brand: " + brand);
        System.out.println("================================");
    }
}

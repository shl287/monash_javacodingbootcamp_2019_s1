
public class Car
{
    private int plateNumber;
    private int travelDistance; 
    private int currentFuelLevel;
    
    public Car(int newPlateNumber){
        plateNumber = newPlateNumber;
        travelDistance = 0;
        currentFuelLevel = 5;
    }    
    
    public void driveWithNoLoop(int distanceToDrive){
        printCarStatus();

        System.out.println("Start driving...");
        for (int i = 0; i < distanceToDrive; i++){
            travelDistance++; // increase distance by 1 for each km driven
            currentFuelLevel--; // decrease fuel level by 1 for each km driven
            System.out.println("The car just travelled 1 km...");
            printCarStatus();
        }
        System.out.println("finish driving...");
    }
    
    public void driveWithForLoop(int distanceToDrive){
        printCarStatus();

        System.out.println("Start driving...");
        for ( int i = 0; i < distanceToDrive; i++ ){
            // increase distance by 1 for each km driven
            travelDistance++; 
            // decrease fuel level by 1 for each km driven
            currentFuelLevel--; 
            System.out.println("The car travelled 1 km...");
            printCarStatus();
        }
        System.out.println("finish driving...");
    }

    public void driveWithWhileLoop(int distanceToDrive){
        printCarStatus();
        
        System.out.println("Start driving...");
        int distanceDriven = 0;
        while ( distanceDriven < distanceToDrive && currentFuelLevel > 0 ){
            travelDistance++; // increase distance by 1 for each km driven
            currentFuelLevel--; // decrease fuel level by 1 for each km driven
            System.out.println("The car just travelled 1 km...");
            printCarStatus();
            distanceDriven++;
        }
        System.out.println("finish driving...");
        
        if (distanceDriven < distanceToDrive && currentFuelLevel == 0)
            System.out.println("The car ran out of fuel and stopped");
        else if (distanceDriven < distanceToDrive && currentFuelLevel != 0)
            System.out.println("The car somehow brokedown half way with fuel remaining");
        else if (distanceDriven == distanceToDrive && currentFuelLevel == 0)
            System.out.println("The car arrived at destiny and also ran out of fuel");
        else
            System.out.println("The car arrived at destiny with fuel remaining");
    }    
    
    public int getPlateNumber(){
        return plateNumber;
    }
    
    public void printCarStatus(){
        System.out.println("-------------------------");
        System.out.println("plateNumber = " + plateNumber);
        System.out.println("travelDistance = " + travelDistance + " km");
        System.out.println("currentFuelLevel = " + currentFuelLevel + " L");
        System.out.println("-------------------------");
    }
}

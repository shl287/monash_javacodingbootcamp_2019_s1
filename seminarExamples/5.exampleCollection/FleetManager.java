import java.util.ArrayList;

/**
 * manage all the cars in the fleet
 */
public class FleetManager
{
    private ArrayList<Car> fleet;
    private int nextPlateNumber;
    
    public FleetManager(){
        fleet = new ArrayList<Car>();
        nextPlateNumber = 1;
    }

    public FleetManager(int initialFleetSize){
        fleet = new ArrayList<Car>(initialFleetSize);
        nextPlateNumber = 1;
        for (int i = 0; i < initialFleetSize; i++){
            fleet.add(new Car(nextPlateNumber));
            nextPlateNumber++;
        }
    }
    
    public void addNewCar(){
        Car newCar = new Car(nextPlateNumber);
        nextPlateNumber++;
        fleet.add(newCar);
    }
    
    public Car findCarByPlateNumber(int plateNumber){
        Car carToFind = null;
        for (int i = 0; i < fleet.size(); i++){
            Car currentCar = fleet.get(i);
            if (currentCar.getPlateNumber() == plateNumber)
                carToFind = currentCar;
        }
        return carToFind;
    }
    
    public void removeOldestCarInFleet(){
        fleet.remove(0);
    }
    
    public int getFleetSize(){
        return fleet.size();
    }
    
    public void printAllCarsInFleet(){
        for (Car car : fleet)
            car.printCarStatus();
    }
}


public class Car
{
    private int currentSpeed;
    
    public Car(){
        currentSpeed = 0;
    }

    public int getCurrentSpeed(){
        return currentSpeed;
    }

    public void setCurrentSpeed(int newCurrentSpeed){
        currentSpeed = newCurrentSpeed;
    }
    
    public int accelarate(int accelaratingTime){
        //the speed added on top of current speed
        int addedSpeed;
        
        //add 2km/h for each second the car accelerates
        addedSpeed = accelaratingTime * 2;  
        
        //update currentSpeed
        currentSpeed = currentSpeed + addedSpeed;
        
        return currentSpeed;
    }

    public int brake(int brakingTime){
        //implementation omitted
        return currentSpeed;
    }
}

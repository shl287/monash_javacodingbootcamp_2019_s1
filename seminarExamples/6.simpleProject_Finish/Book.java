public class Book
{
    private String title;
    private double price;
    
    public Book(){
        title = "unknown";
        price = -1;
    }
    
    public Book(String newTitle, double newPrice){
        title = newTitle;
        price = newPrice;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void setTitle(String newTitle){
        title = newTitle;
    }
    
    public double getPrice(){
        return price;
    }
    
    public void setPrice(double newPrice){
        price = newPrice;
    }
    
    public void printBookDetail(){
        System.out.println("Title: " + title);
        System.out.println("Price: " + price);
    }
}

import java.util.ArrayList;

public class Collector
{
    private String collectorName;
    private ArrayList<Book> bookCollection;
    
    public Collector(){
        collectorName = "unknown";
        bookCollection = new ArrayList<Book>();
    }
    
    public Collector(String newCollectorName, ArrayList<Book> newBookCollection){
        collectorName = newCollectorName;
        bookCollection = newBookCollection;
    }

    private String getcollectorName(){
        return collectorName;
    }
    
    private void setcollectorName(String newcollectorName){
        collectorName = newcollectorName;
    }
   
    private ArrayList<Book> getBookCollection(){
        return bookCollection;
    }
    
    private void setBookCollection(ArrayList<Book> newBookCollection){
        bookCollection = newBookCollection;
    }
    
    public void collectANewBook(String title, double price){
        bookCollection.add(new Book(title, price));
    }
    
    public void removeBookByTitle(String title){
        boolean titleFound = false;
        int searchIndex = 0;
        while (!titleFound && searchIndex != bookCollection.size()){
            if (bookCollection.get(searchIndex).getTitle().equals(title)){
                titleFound = true;
                bookCollection.remove(searchIndex);
            }
            searchIndex++;
        }
    }
    
    public void printAverageBookPrice(){
        double averagePrice = 0;
        int bookCount = bookCollection.size();
        double totalPrice = 0;
        for (int i = 0; i < bookCount; i++)
            totalPrice += bookCollection.get(i).getPrice();
        averagePrice = totalPrice / bookCount;
        System.out.println("Average Book Price: $" + averagePrice);
    }
    
    public void printBookCollection(){
        System.out.println("Collector name: " + collectorName);
        if (bookCollection.size() == 0)
            System.out.println("the collection is empty!");
        else {
            System.out.println("==== Book Collection Start ====");
            for (Book book : bookCollection)
                book.printBookDetail();
            System.out.println("==== Book Collection End ====");
        }
    }
}

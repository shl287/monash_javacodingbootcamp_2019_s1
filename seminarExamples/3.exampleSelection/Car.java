
public class Car
{
    private int currentSpeed;
    
    public Car(){
        currentSpeed = 0;
    }

    public Car(int initialSpeed){
        currentSpeed = initialSpeed;
    }
    
    public int getCurrentSpeed(){
        return currentSpeed;
    }

    public void setCurrentSpeed(int newCurrentSpeed){
        currentSpeed = newCurrentSpeed;
    }
    
    public int accelarate(int accelaratingTime){
        //the rate at which the car accelarates
        int accelerationRate = 2;
        
        int addedSpeed = accelaratingTime * accelerationRate;  
        
        currentSpeed = currentSpeed + addedSpeed;
        
        return currentSpeed;
    }

    public int brake(int brakingTime){
        //the rate at which the car accelarates
        int decelerationRate = 2;
        
        int reducedSpeed = brakingTime * decelerationRate;
        
        currentSpeed = currentSpeed - reducedSpeed;
        
        return currentSpeed;
    }

    public int brakeCorrectly(int brakingTime){
        //the rate at which the car accelarates
        int decelerationRate = 2;
        
        int reducedSpeed = brakingTime * decelerationRate;
        
        if (currentSpeed - reducedSpeed > 0)
            currentSpeed = currentSpeed - reducedSpeed;
        else
            currentSpeed = 0;

        return currentSpeed;
    }

    public int brakeWithAutoEngineStop(int brakingTime){
        int decelerationRate = 2;
        
        int reducedSpeed = brakingTime * decelerationRate;
        
        if (currentSpeed - reducedSpeed > 0)
            currentSpeed = currentSpeed - reducedSpeed;
        else
            currentSpeed = 0;
            System.out.println("The engine has stopped");

        return currentSpeed;
    }

    public int brakeWithAutoEngineStopCorrectly(int brakingTime){
        int decelerationRate = 2;
        
        int reducedSpeed = brakingTime * decelerationRate;
        
        if (currentSpeed - reducedSpeed > 0)
            currentSpeed = currentSpeed - reducedSpeed;
        else
        {
            currentSpeed = 0;
            System.out.println("The engine has stopped");
        }

        return currentSpeed;
    }

    public int brakeWithLongBrakingTime(int brakingTime){
        int decelerationRate = 2;
        
        int reducedSpeed = brakingTime * decelerationRate;
        
        if (currentSpeed - reducedSpeed > 0 && brakingTime > 10)
        {
            currentSpeed = currentSpeed - reducedSpeed;
            System.out.println("Engine brake engaged");
        }
        else if (currentSpeed - reducedSpeed > 0 && brakingTime <= 10)
            currentSpeed = currentSpeed - reducedSpeed;
        else
        {
            currentSpeed = 0;
            System.out.println("The engine has stopped");
        }

        return currentSpeed;
    }
    
    public void printCarStatus(){
        System.out.println("currentSpeed = " + currentSpeed);
    }
}

// TODO: import the class you would be using at the start of your source code

public class Collector
{
    // TODO: declare the 2 fields of the Collector class
    //          1) name of the collector as a string, 
    //          2) the ArrayList containing his book collection 
    
    
    public Collector(){
        //TODO: the default constructor, it should initialize all the fields to a sensible value
    }

    public Collector(String newCollectorName, ArrayList<Book> newBookCollection){
        //TODO: the non-default constructor, it should allow the user to customize 
        //      the value of all the fields
    }
    
    // TODO: create accessor method for the name of the collector

    
    // TODO: create mutator method for the name of the collector
    

    // TODO: create accessor method for the ArrayList of books
    

    // TODO: create mutator method for the ArrayList of books
    
    
    public void collectANewBook(String title, double price){
        // TODO: create a new book based on the actual value of the parameters
        //       add the newly created book into the bookCollection by calling the add() 
        //       method in the ArrayList class
    }
    
    public void removeBookByTitle(String title){
        // TODO: loop through the collection of the book, keep looping as long as 
        //          a. the book with the title has not been found yet, AND
        //          b. there are still books in the collection hasn't been checked
        //       for each book:
        //          1. get its title
        //          2. check if its title match the title we are looking for
        //              2.1. if the title match, remove the book from the collection 
        //                  by calling the remove() method in the ArrayList class
        //              2.2. if the title does not match, do nothing for this book
    }
    
    public void printAverageBookPrice(){
        // TODO: get the total number of books in the collection
        //       define and initialize the total price for all books as 0
        //       loop through the entire book collection, for each book:
        //          1. get the price of the book by calling the accessor method of price
        //          2. add the price of this book to the total price
        //       calculate average price by deviding the totalprice with total number of books
        //       print the average price of books on the console
    }
    
    public void printBookCollection(){
        // TODO: print the name of the collector
        //       if there is no book in the collection, print an error message
        //       otherwise, loop through the entire book collection 
        //       for each book
        //          1. print the book detail by calling the printBookDetail method from book class
    }
}

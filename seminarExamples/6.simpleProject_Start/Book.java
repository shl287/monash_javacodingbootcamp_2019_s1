public class Book
{
    /**
     * this is the default constructor, each class should have a default constructor
     * the default constructor should initialize all the fields to a reasonable value
     */
    public Book(){
        //TODO: put your code here
    }
    
    /**
     * this is the non-default constructor, if a class has field, it should has at least
     * one non-default constructor apart from default constructor. The non-default 
     * constructor should allow it user to customize the initial value of the fields
     */
    public Book(String newTitle, double newPrice){
        //TODO: put your code here
    }
    
    /**
     * accessor of title field
     */
    public String getTitle(){
        //TODO: put your code here
    }
    
    /**
     * mutator of title field
     */
    public void setTitle(String newTitle){
        //TODO: put your code here
    }
    
    /**
     * accessor of price field
     */
    public double getPrice(){
        //TODO: put your code here
    }
    
    /**
     * mutator of price field
     */
    public void setPrice(double newPrice){
        //TODO: put your code here
    }
    
    /**
     * a method to display the book information on the screen
     */
    public void printBookDetail(){
        //TODO: put your code here
    }
}

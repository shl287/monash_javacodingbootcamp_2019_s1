
public class Car
{
    private int currentSpeed;
    private String brand;
    
    public Car(){
        currentSpeed = 0;
        brand = "Holden";
    }

    public Car(int initialSpeed, String initialBrand){
        currentSpeed = initialSpeed;
        brand = initialBrand;
    }
    x
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String newBrand){
        brand = newBrand;
    }
    
    public int getCurrentSpeed(){
        return currentSpeed;
    }

    public void setCurrentSpeed(int newCurrentSpeed){
        currentSpeed = newCurrentSpeed;
    }
    
    public int accelarate(int accelaratingTime){
        //the rate at which the car accelarates
        int accelerationRate = 2;
        
        //the additional speed the car gains depends on the
        //accelarationTime and the accelarationRate
        int addedSpeed = accelaratingTime * accelerationRate;  
        
        currentSpeed = currentSpeed + addedSpeed;
        
        return currentSpeed;
    }
    
    public void printCarStatus(){
        System.out.println("currentSpeed = " + currentSpeed);
        System.out.println("brand = " + brand);
    }
}
